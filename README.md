# KYC

## Description
The Know Your Customer microservice for DrugDev.

## Install
```
venv -p `which python3` .venv
source .venv/bin/activate
pip install -e .
```
can also install `notify` extras for local desktop notifications:
```
pip install -e .[notify]
```

## Maintenance
### List available maintenance commands
```
inv list
```

### Run dev server
```
./main.py
```

### Run tests
```
pytest -v
```

# Not needed for test
## Manually run local containers
```
docker rm -fv assets_service_kyc assets_rabbitmq assets_worker_kyc

docker run -d --name assets_service_kyc \
       -p 8096:80 \
       -v /opt/blockex/services/kyc/config.yaml:/opt/blockex/services/kyc/config.yaml \
       blockexservices.azurecr.io/service_kyc:latest
docker network connect services assets_service_kyc

docker run -d --name assets_rabbitmq --restart always \
       -p 5672:5672 --expose 5672 rabbitmq:latest
docker network connect services assets_rabbitmq

docker run -d --name assets_worker_kyc \
       -p 6899-6903:6899-6903 \
       -v /opt/blockex/services/kyc/config.yaml:/opt/blockex/services/kyc/config.yaml \
       blockexservices.azurecr.io/worker_kyc:latest \
       celery -A service_kyc.celery_tasks worker
docker network connect services assets_worker_kyc

docker network connect services postgres
```
