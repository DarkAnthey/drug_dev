import unittest
import json

from service_kyc import util
from service_kyc.extensions import db


class TestEndpoints(unittest.TestCase):
    def setUp(self):
        self.app = util.get_application('test_kyc_app', test=True, register_blueprint=True)
        self.app.config["SQLALCHEMY_DATABASE_URI"] = f"sqlite://"
        db.init_app(self.app)

        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

        self.client = self.app.test_client()

    def tearDown(self):
        db.drop_all()
        self.app_context.pop()
