import json

from tests.endpoints_test_setup import TestEndpoints
from http import HTTPStatus
from service_kyc.models import User, Email


class TestUserList(TestEndpoints):
    def setUp(self):
        super(TestUserList, self).setUp()

    def create_user(self, data):
        response = self.client.post('/user/', data=json.dumps(data), content_type='application/json')
        response_code = response.status_code
        assert response_code == HTTPStatus.OK

    def test_user_add_user(self):
        data = {'username': 'aaa', 'email': 'test@xxx.com'}
        self.create_user(data)
        all_users = User.query.all()
        assert len(all_users) == 1

    def test_user_get_user_detail(self):
        data = {'username': 'aaa', 'email': 'test@xxx.com'}
        self.create_user(data)
        all_users = User.query.all()
        user_id = all_users[0].id
        response = self.client.get('/user/%s' % user_id,  content_type='application/json')
        response_code = response.status_code
        assert response.json.get('username') == 'aaa'

    def test_user_delete_user(self):
        data = {'username': 'aaa', 'email': 'test@xxx.com'}
        self.create_user(data)
        all_users = User.query.all()
        assert len(all_users) == 1

        user_id = all_users[0].id
        response = self.client.delete('/user/%s' % user_id,  content_type='application/json')
        response_code = response.status_code
        assert response_code == HTTPStatus.NO_CONTENT
        all_users = User.query.all()
        assert len(all_users) == 0

    def test_user_get_all_user(self):
        data = {'username': 'aaa', 'email': 'aaa@xxx.com'}
        self.create_user(data)
        data = {'username': 'bbb', 'email': 'bbb@xxx.com'}
        self.create_user(data)

        all_users = User.query.all()
        assert len(all_users) == 2
        response = self.client.get('/user/', content_type='application/json')
        response_code = response.status_code
        assert response_code == HTTPStatus.OK
        ids = [u.get('username') for u in response.json]
        assert ids == ['aaa', 'bbb']
