import json

from tests.endpoints_test_setup import TestEndpoints
from http import HTTPStatus
from service_kyc.models import Email


class TestEmailList(TestEndpoints):
    def setUp(self):
        super(TestEmailList, self).setUp()

        data = {'username': 'aaa', 'email': 'test@xxx.com'}

        response = self.client.post('/user/', data=json.dumps(data), content_type='application/json')
        response_code = response.status_code
        assert response_code == HTTPStatus.OK

    def create_email(self, data):
        response = self.client.post('/user/email/', data=json.dumps(data), content_type='application/json')
        response_code = response.status_code
        assert response_code == HTTPStatus.OK

    def test_email_add_email(self):
        data = {'email': 'test@xxx.com', 'user_id': 1}
        self.create_email(data)
        all_emails = Email.query.all()
        assert len(all_emails) == 1

    def test_email_get_email_detail(self):
        data = {'email': 'test@xxx.com'}
        self.create_email(data)
        all_emails = Email.query.all()
        email_id = all_emails[0].id
        response = self.client.get('/email/%s' % email_id,  content_type='application/json')
        response_code = response.status_code
        assert response.json.get('emai') == 'test@xxx.com'

    def test_email_delete_email(self):
        data = {'email': 'test@xxx.com'}
        self.create_email(data)
        all_emails = Email.query.all()
        assert len(all_emails) == 1

        email_id = all_emails[0].id
        response = self.client.delete('/email/%s' % email_id,  content_type='application/json')
        response_code = response.status_code
        assert response_code == HTTPStatus.NO_CONTENT
        all_emails = Email.query.all()
        assert len(all_emails) == 0

    def test_email_get_all_email(self):
        data = {'email': 'aaa@xxx.com'}
        self.create_email(data)
        data = {'email': 'bbb@xxx.com'}
        self.create_email(data)

        all_emails = Email.query.all()
        assert len(all_emails) == 2
        response = self.client.get('/email/', content_type='application/json')
        response_code = response.status_code
        assert response_code == HTTPStatus.OK
        ids = [u.get('email') for u in response.json]
        assert ids == ['aaa@xxx.com', 'bbb@xxx.com']
