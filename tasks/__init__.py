from invoke import task, Collection

from tasks.db import db_collection
from tasks.tests import tests_collection

ns = Collection()
ns.add_collection(db_collection)
ns.add_collection(tests_collection)
