import os
import contextlib
from invoke import task, Collection

import service_kyc.models as kyc_models
import service_kyc.util as util

db_collection = Collection('db')

@task
def generate(ctx):
    """ Invoke alembic to generate new migration from the existing db
        and service' models.
    """
    ctx.run('alembic revision --autogenerate')
db_collection.add_task(generate)


@task
def up(ctx, test=False):
    """ Invoke alembic to migrate db up to current HEAD.
    """
    if test:
        ctx.run('alembic -x test_db=True upgrade head')
    else:
        ctx.run('alembic upgrade head')
db_collection.add_task(up)


@task
def down(ctx, test=False):
    """ Invoke alembic to migrate db down to base.
    """
    if test:
        ctx.run('alembic -x test_db=True downgrade base')
    else:
        ctx.run('alembic downgrade base')
db_collection.add_task(down)
