from invoke import task, Collection, call

from tasks import db

tests_collection = Collection('tests')


@task
def pretty(ctx, lint=False, style=False):
    """ Run pycodestyle (--style), pylint (--lint) or both (default) on the tests.
    """

    all = not style and not lint

    if style or all:
        ctx.run('pycodestyle --config=pystyle.cfg ./service_kyc/ ', warn=True)
        ctx.run('pycodestyle --config=pystyle.cfg ./tests/ ', warn=True)
    if lint or all:
        ctx.run('pylint ./service_kyc/', warn=True)
        ctx.run('pylint ./tests/', warn=True)

tests_collection.add_task(pretty)


@task
def list(ctx, show_names=True):
    """ Get all test names
        if run from command - print tests names
        else return to parent method array ot test names
    """
    result = ctx.run('pytest --verbosity=2 --collect-only', hide=True)
    tests = []
    stdout = result.stdout if result.stdout else result.stderr
    current_module, current_testcase, current_testfunction = '', '', ''
    for line in stdout.split('\n'):
        current_line = line.strip().replace("'", "").replace("", "")\
                           .replace(">", "").replace("<", "").split()
        if current_line and len(current_line) == 2:
            current_module = current_line[1] if current_line[0] == 'Module' else current_module
            current_testcase = current_line[1] if current_line[0] == 'UnitTestCase' else current_testcase
            if current_line[0] == 'TestCaseFunction':
                current_testfunction = current_line[1]
                tests.append(f"{current_module}::{current_testcase}::{current_testfunction}")

    if show_names:
        print('\n--------- APP TESTS ---------\n')
        for test in tests:
            print(test)
        print('\n-----------------------------\n')
    return tests

tests_collection.add_task(list)


@task
def case(ctx, case, index=0, tc=False):
    """ Run singe test case with db create/drop
        Usage: invoke tests.case -c <name test case>

        test case name format:
            - tests/test_folder/test_file.py
            - tests/test_folder/test_file.py::TestClass
            - tests/test_folder/test_file.py::TestClass::test_method
    """
    ctx.run(f'pytest {case}', hide=False, warn=True)

tests_collection.add_task(case)


@task
def run(ctx, tc=False):
    """ Run all tests native
    """
    ctx.run('pytest -n auto ./tests ', hide=False, warn=True)


tests_collection.add_task(run)


@task
def all(ctx):
    """Run all checks at once."""

    pretty(ctx)
    run(ctx)

tests_collection.add_task(all, default=True)
