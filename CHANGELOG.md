#### 0.12.8
- Update changelog (KYC-311)
- Hotfix retrieve a GBG reports (KYC-308)
#### 0.12.8
- Filter for select countries (KYC-281)
#### 0.12.6
- Fix bug retrieving country list during GBG verification (KYC-271)
#### 0.12.5
- Remove environment configuration for broker_url
- Enable flask migrate for database migration  (KYC-236)
- General refactoring.
- Switch Tests database to SQLite (KYC-236)
- Create starter file for Celery worker (KYC-257)
- Fix health-check Celery error (KYC-88)
#### 0.12.4
- GDPR Delete user information (KYC-222)
#### 0.12.3
- Make auto_verified nullable in model. Update migration to exclude auto_verified conversion to NON-NULLABLE (KYC-226).
#### 0.12.2
- Fix bug with Permission ID's returned by user that cannot be mapped (OPS-993)
- Configure authentication using blockex-tools
- Remove user views and stored procedures
- Suppress header parsing error in GBG calls
- Audit log GBG approval and rejection of fields (KYC-86, KYC-218)

#### 0.12.1
- Implement status using Database Column (Deprecate User view)
- Fix bug with calculating status for new KYC Profiles and Users (KYC-186)
- Fix bug with sort order of profiles returned for user. (KYC-187)
- Implements stored procedures for status computation.
- Implements views for user, user_profile, and user_field tables respectively.
- Implement Alembic Extensions to support migration of stored procedures and views.
- Implements filters on the /user endpoint. (KYC-116)

#### 0.12.0
- Implement IN_ESCALATION status for KYC
- Whitelist case allocator permissions to Retrieve users, fields and update fields
#### 0.11.3
-  Replace local path with octopus variable
#### 0.11.2
-  Upgrade celery version to 4.2.0
#### 0.11.1
- Implements pagination on user endpoint

#### 0.11.0
- Included Changelog endpoint from blockex-tools
- Included swagger endpoint from blockex-tools
- Configured logging using blockex-tools
- Includes status in user get.
- Configure error handlers from blockex-tools
- Update to Batch Audit Log API
- Switch from nosetests to pytest
- Update flask version to version 1.x

#### 0.10.1

- Fix broken usage of blockex.flask libraries
- Updated minimum dependency of blockex-tools to 0.2.0

#### 0.10.0

- Endpoints for all User fields update and retrieval
- Endpoint for retrieving user files and passport
- Endpoint for retrieving GBG response
- Refactoring changes in models and tests
- Replace util.error_response with abort
- Specify default handlers for 400, 401 and 404 responses
- Squashed migrations
- Fixed computation of GBG maximum file size
- Includes Id in user detail response

#### 0.9.1

- Disables GBG validation

#### 0.9.0

- Replaced SQLAlchemy with Flask-Sqlalchemy
- Introduced dependency on Audit Logging service
- Fixed out of sync updates to BO API introduced by queue_all_status
- Introduced Celery task wrapper for config-driven deactivation
- Replaced Celery usage with Flask Wrapped celery task context
- County was made an optional KYC field
- GBG validation was introduced for KYC Identification verification
- General refactoring to Improve code quality.
- Introduced dependency on Blockex-tools
- Implements 3 layer sub-folder representation for file storage
- IN_REVIEW status introduced for auto-verified KYC requests
- Introduces auto-verification for Firstname, Lastname, DOB and Country fields

#### 0.8.1
- switched to base image using Python 3.6

#### 0.8.0
- Fixed multiple queue creation

#### 0.7.6
- Implements deployable config

#### 0.7.5
- Updated dockerhub and base image name

#### 0.7.4
- Resolved multiple queue creation from celery

#### 0.7.3
- switched from with permission slugs to permission ids
- changed application log format
- included optional field in kyc
- changed passport field type to passport
- changed computation of status
- celery usage mocked out in unit tests

#### 0.7.2
- revert passport field type back to file

#### 0.7.1
- fix BACKEND-800 undefined status issue
- no longer cause startup failure on missing auth config
- make celery and rmq monitoring work
- load auth from places other than URL

#### 0.7.0
- in-progress status added to BO API integration
- GBG tasks refactored for code cleanup
- datetime JSON handling

#### 0.6.1
- dev deployment for worker

#### 0.6.0
- unused celery code removed
- BO API integration added

#### 0.5.15
- GBG passport hardcoded field doesnt interfere with the regular processing

#### 0.5.14
- GBG integration buildout
- celery tasks framework
- BACKEND-629 - bad permission handling in file download

#### 0.5.13
- BDR ENUM migration fix
- unicode filename handling

#### 0.5.12
- data load hotfix

#### 0.5.11
- trader data fixes
- basic logging

#### 0.5.10
- optional explicit keys for data entities
- new field types
- trader data keys

#### 0.5.9
- postgres BDR migrations fix

#### 0.5.8
- permission tests added
- correct permission handling for file delivery

#### 0.5.7
- trader profiles data fix

#### 0.5.6
- trader profiles data fix

#### 0.5.5
- declaration field type fix

#### 0.5.4
- full trader profiles prod data

#### 0.5.3
- CORS support removed
- docs fixes

#### 0.5.2
- partner_id voltdb bigint fix

#### 0.5.1
- vvv task
- bugfixes

#### 0.5.0
- user list endpoint added
- user details endpoint added

#### 0.4.0
- status update endpoints added
- permission overlap bug fixed
- alembic log level adjusted
- data representations added
- request data validation added
- swagger docs updated

#### 0.3.11
- trader role  added to prod data

#### 0.3.10
- trader profile definitions added to prod data

#### 0.3.9
- fields links removed from prod data

#### 0.3.8
- sort order added to fixtures

#### 0.3.7
- notify import error handling
- build base now used

#### 0.3.6
- dbus removed from core dependencies

#### 0.3.5
- minor code cleanup
- lender KYC data

#### 0.3.4
- role slug dash to underscore fix

#### 0.3.3
- deployment task

#### 0.3.2
- volt_id bigint fix
- unique name constraint fix

#### 0.3.1
- file upload implemented

#### 0.3.0
- roles and permissions

#### 0.2.1
- KYC prod data fixtures

#### 0.2.0
- alembic
- maintenance tasks
- profile endpoints
- config provider
- CORS

#### 0.1.0
- remember, no django

#### 0.0.1
- initial version
