#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os

base_path = os.path.abspath(os.path.dirname(__file__))
from service_kyc.app import kyc_app as flask_app

if __name__ == '__main__':
    dev_port = 8000
    flask_app.run(port=dev_port)
