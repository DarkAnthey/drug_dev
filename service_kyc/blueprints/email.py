import json
from http import HTTPStatus
from service_kyc.extensions import db

from flask import Blueprint, jsonify, request
from service_kyc.models import User, Email

BASE_URL = '/user/email'
email_bp = Blueprint(name='email', import_name=__name__, url_prefix=BASE_URL)

EMAIL_LIST_VIEW = {"rule": '', "methods": ['GET']}
EMAIL_DETAILS_VIEW = {"rule": '/<int:id>', "methods": ['GET']}
EMAIL_CREATE = {"rule": '/<int:id>', "methods": ['POST']}
EMAIL_UPDATE = {"rule": '/<int:id>', "methods": ['PUT']}
EMAIL_DELETE = {"rule": '/<int:id>', "methods": ['DELETE']}


@email_bp.route(**EMAIL_LIST_VIEW)
def get_email():
    all_emails = Email.query.all()
    _all_emails = [u.as_dict() for u in all_emails]
    return jsonify(_all_emails), HTTPStatus.OK


@email_bp.route(**EMAIL_CREATE)
def add_email():
    # Here should be checker like WTF form. Can I skip it for test example?
    email = request.json['email']

    new_email = Email(email=email)

    db.session.add(new_email)
    db.session.commit()

    return jsonify(new_email.as_dict()), HTTPStatus.OK


@email_bp.route(**EMAIL_DETAILS_VIEW)
def email_detail(id):
    email = Email.query.filter_by(id=id).first().as_dict()
    return jsonify(email), HTTPStatus.OK


@email_bp.route(**EMAIL_UPDATE)
def email_update(id):
    # Here should be checker like WTF form. Can I skip it for test example?
    email = Email.query.get(id)
    new_email = request.json['email']

    email.email = new_email

    db.session.commit()
    return jsonify(email.as_dict())


@email_bp.route(**EMAIL_DELETE)
def email_delete(id):
    email = Email.query.get(id)
    db.session.delete(email)
    db.session.commit()

    return jsonify(email.as_dict()), HTTPStatus.NO_CONTENT
