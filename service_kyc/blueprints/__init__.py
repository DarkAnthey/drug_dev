from .user import user_bp
from .email import email_bp

all_blueprints = [
    user_bp, email_bp,
]
