import json
from http import HTTPStatus
from service_kyc.extensions import db

from flask import Blueprint, jsonify, request
from service_kyc.models import User, Email

BASE_URL = '/user'
user_bp = Blueprint(name='user', import_name=__name__, url_prefix=BASE_URL)


USER_LIST_VIEW = {"rule": '', "methods": ['GET']}
USER_CREATE = {"rule": '', "methods": ['POST']}
USER_DETAILS_VIEW = {"rule": '/<int:id>', "methods": ['GET']}
USER_DETAILS_BY_EMAIL_VIEW = {"rule": '/email/<email>', "methods": ['GET']}
USER_UPDATE = {"rule": '/<int:id>', "methods": ['PUT']}
USER_DELETE = {"rule": '/<int:id>', "methods": ['DELETE']}


@user_bp.route(**USER_LIST_VIEW)
def get_user():
    all_users = User.query.all()
    _all_users = [u.as_dict() for u in all_users]
    return jsonify(_all_users), HTTPStatus.OK


@user_bp.route(**USER_CREATE)
def add_user():
    # Here should be checker like WTF form. Can I skip it for test example?
    username = request.json['username']
    email = request.json['email']

    exists_select = Email.query.filter_by(email=email).first()
    if exists_select is not None:
        return '', HTTPStatus.CONFLICT

    new_user = User(username)
    new_email = Email(email=email, user_id=new_user.id)
    db.session.add(new_email)
    new_user.emails.append(new_email)

    db.session.add(new_user)
    db.session.commit()
    return jsonify(new_user.as_dict()), HTTPStatus.OK


@user_bp.route(**USER_DETAILS_VIEW)
def user_detail(id):
    user = User.query.filter_by(id=id).first().as_dict()
    return jsonify(user), HTTPStatus.OK


@user_bp.route(**USER_DETAILS_BY_EMAIL_VIEW)
def user_by_email_detail(email):
    email = Email.query.filter_by(email=email).first()
    if email is None:
        return '', HTTPStatus.BAD_REQUEST

    user = User.query.get(email.user_id)
    return jsonify(user), HTTPStatus.OK


@user_bp.route(**USER_UPDATE)
def user_update(id):
    # Here should be checker like WTF form. Can I skip it for test example?
    user = User.query.get(id)
    username = request.json['username']

    user.username = username
    new_user.emails.append(new_email)

    db.session.commit()
    return jsonify(user.as_dict()), HTTPStatus.OK


@user_bp.route(**USER_DELETE)
def user_delete(id):
    user = User.query.get(id)
    db.session.delete(user)
    db.session.commit()

    return jsonify(user.as_dict()), HTTPStatus.NO_CONTENT
