"""
    This class is for celery tasks and periodic
"""
from http import HTTPStatus
from celery.schedules import crontab

from flask import current_app as kyc_app

from service_kyc import util
from service_kyc.models import User, Email
from service_kyc.celery import celery_app
import random, string

logger = get_task_logger(__name__)

def randomword(length):
   letters = string.ascii_lowercase
   return ''.join(random.choice(letters) for i in range(length))


@celery.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    # Calls reverse_messages every 15 seconds.
    sender.add_periodic_task(15.0, reverse_messages, name='reverse every 10')

    # Calls delete email every 60 seconds
    sender.add_periodic_task(60.0, remove_emails, name='Log every 30')

@celery.task
def add_email():
    # Add this email to redis for save stored email by periodic task
    email1 = Email(email=randomword(8)+'@xxx.com', user_id=1)
    email2 = Email(email=randomword(8)+'@xxx.com', user_id=1)
    db.session.add(email1)
    db.session.add(email2)
    db.session.commit()

@celery.task
def remove_emails():
    # Get email from redis created by periodic task
    Email.query.filter_by(user_id=1).delete()
    db.session.commit()
