from http import HTTPStatus

CHANGELOG = 'CHANGELOG.md'

MESSAGE_KEY = "msg"

USER_NOT_FOUND = "User not found"
BAD_REQUEST = "Bad request"


MESSAGES = {
    "bad_request": {
        "message": {MESSAGE_KEY: BAD_REQUEST},
        "status_code": HTTPStatus.BAD_REQUEST
    },
    "user_not_found": {
        "message": {MESSAGE_KEY: USER_NOT_FOUND},
        "status_code": HTTPStatus.NOT_FOUND,
    },
}
