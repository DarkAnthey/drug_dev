from service_kyc.extensions import db


class User(db.Model):
    __tablename__ = 'user'

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80), unique=True)
    emails = db.relationship('Email', backref='user', lazy='dynamic')

    def __init__(self, username):
        self.username = username

    def as_dict(self):
       return {c.name: getattr(self, c.name) for c in self.__table__.columns}
