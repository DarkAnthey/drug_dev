from service_kyc.extensions import db

class Email(db.Model):
    __tablename__ = 'email'

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(255), unique=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    def __init__(self, email, user_id):
        self.email = email
        self.user_id = user_id

    def as_dict(self):
       return {c.name: getattr(self, c.name) for c in self.__table__.columns}
