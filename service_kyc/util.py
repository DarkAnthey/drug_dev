import os
from functools import wraps

from flask import current_app as kyc_app, Flask
from service_kyc.extensions import db, migrate


def get_application(name='kyc', **kwargs):
    kyc_app = Flask(name)

    test = kwargs.get('test', False)
    kyc_app.config['TESTING'] = test

    apply_configuration(kyc_app)
    register_extensions(kyc_app)

    if 'register_blueprint' not in kwargs or kwargs.get('register_blueprint'):
        register_blueprints(kyc_app)

    kyc_app.logger.debug(f'local config loaded')
    return kyc_app


def register_extensions(flask_app):
    db.init_app(flask_app)
    migrate.init_app(flask_app, db)


def initialize_celery(flask_app):
    celery_config = {
        'task_routes': {
            'service_kyc.celery_tasks.notify_status_change': {'queue': 'status_change'},
        },
        'task_default_queue': 'kyc_celery',
        'task_queue_max_priority': 10,
        'task_ignore_result': True,
        'task_acks_late': True,
        'imports': [
            'service_kyc.celery_tasks',
        ],
    }

    # BROKER_URL is celery broker url
    env_broker_url = os.environ.get('BROKER_URL')
    if env_broker_url:
        celery_config['broker_url'] = env_broker_url
        flask_app.logger.warning('Celery Configured from Environment Variable ')

    flask_app.config.update(celery_config)


def get_celery_application(kyc_app):
    from celery import Celery
    # Configs shoud be in config file.
    REDIS_HOST = "0.0.0.0"
    REDIS_PORT = 6379
    BROKER_URL = environ.get('REDIS_URL', "redis://{host}:{port}/0".format(
        host=REDIS_HOST, port=str(REDIS_PORT)))
    CELERY_RESULT_BACKEND = BROKER_URL

    celery = Celery(
        app.import_name,
        broker=BROKER_URL
    )
    TaskBase = celery.Task

    class ContextTask(TaskBase):
        abstract = True

        def __call__(self, *args, **kwargs):
            with kyc_app.app_context():
                return TaskBase.__call__(self, *args, **kwargs)

    celery.Task = ContextTask
    return celery


def register_blueprints(app):
    from service_kyc.blueprints import all_blueprints

    for item in all_blueprints:
        app.register_blueprint(item)


def apply_configuration(flask_app):
    """apply app configuration from the config files"""

    flask_app.url_map.strict_slashes = False
    flask_app.debug = False

    basedir = os.path.abspath(os.path.dirname(__file__))
    flask_app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'crud.sqlite')

    initialize_celery(flask_app)


def is_enabled_task(task_method):
    @wraps(task_method)
    def is_enabled_task_func(*args, **kwargs):
        # Task enable by default
        result = task_method(*args, **kwargs)
        return result

    return is_enabled_task_func
