from .util import get_celery_application, get_application

kyc_app = get_application(__name__, register_blueprint=False)
celery_app = get_celery_application(kyc_app)
