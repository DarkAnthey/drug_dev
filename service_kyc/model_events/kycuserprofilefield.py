from sqlalchemy import event
from sqlalchemy.orm import Session
from service_kyc.models import User

@event.listens_for(User, 'before_delete')
def receive_after_delete(mapper, connection, target):
    # Invalidate cache here
    update_user_and_profile_status(connection, target.user_profile_id)
