EXPOSE 80
ADD . /opt/drugdev/services/users/
VOLUME /opt/drugdev/services/users/uploads
WORKDIR /opt/drugdev/services/users
RUN pip3.6 install .
CMD gunicorn -w1 -b0.0.0.0:80 service_users.app:users_app
