import setuptools
import os
from setuptools import setup

with open('requirements.txt') as f:
    required = f.read().splitlines()

version = "0.0.1"

install_requires = required

setuptools.setup(
    name="service_kyc",
    version=version,
    packages=(
        "service_kyc",
        "service_kyc.models",
        "service_kyc.model_events",
        "service_kyc.alembic_extensions",
    ),
    install_requires=install_requires,
    entry_points={
        "console_scripts": [
        ],
    },
)
